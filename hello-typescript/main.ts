
// JS purto
var minhaVar = 'minha variável';

function minhaFunc(x, y) {
   return x + y;
}


// ECMAScript6 
let num = 2;
const PI = 3.14;

// ---- Arrow functions --- 
// JS PURO
var numeros = [1, 2, 3];
numeros.map(function(valor) {
   return valor * 2;
});

// ES 6
numeros.map(valor => {
   return valor * 2;
});
// OU
numeros.map(valor => valor * 2);

numeros.map(valor => {
   return valor * 2;
});
// OU
numeros.map(valor => valor * 2);

class Matematica {
   soma(x, y) {
      return x + y;
   }
}

// Tipagem no TS
var n1: any = 'sdasdasd';
n1 = 4;

