// JS purto
var minhaVar = 'minha variável';
function minhaFunc(x, y) {
    return x + y;
}
// ECMAScript6 
var num = 2;
var PI = 3.14;
// ---- Arrow functions --- 
// JS PURO
var numeros = [1, 2, 3];
numeros.map(function (valor) {
    return valor * 2;
});
// ES 6
numeros.map(function (valor) {
    return valor * 2;
});
// OU
numeros.map(function (valor) { return valor * 2; });
numeros.map(function (valor) {
    return valor * 2;
});
// OU
numeros.map(function (valor) { return valor * 2; });
var Matematica = /** @class */ (function () {
    function Matematica() {
    }
    Matematica.prototype.soma = function (x, y) {
        return x + y;
    };
    return Matematica;
}());
// Tipagem no TS
var n1 = 'sdasdasd';
n1 = 4;
