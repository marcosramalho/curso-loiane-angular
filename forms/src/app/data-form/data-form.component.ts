import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Observable, empty } from 'rxjs';

import { DropdownService } from '../shared/services/dropdown.service';
import { EstadoBr } from '../shared/models/estadobr';
import { ConsultaCepService } from '../shared/services/consulta-cep.service';
import { FormValidations } from '../shared/form-validations';
import { VerificaEmailService } from './services/verifica-email.service';
import { map, tap, distinctUntilChanged, switchMap } from 'rxjs/operators';

import { BaseFormComponent } from '../shared/base-form/base-form.component';
import { Cidade } from '../shared/models/cidade';


@Component({
  selector: 'app-data-form',
  templateUrl: './data-form.component.html',
  styleUrls: ['./data-form.component.css']
})
export class DataFormComponent extends BaseFormComponent implements OnInit {

  //public form: FormGroup;
  public estados: EstadoBr[];
  //public estados: Observable<EstadoBr[]>;
  public cidades: Cidade[];
  public cargos: any[];
  public tecnologias: any[];
  public newsletterOp: any[];
  public frameworks = ['Angular', 'React', 'Vue', 'Sencha'];

  constructor(private formBuilder: FormBuilder,
    private http: HttpClient,
    private dropdownService: DropdownService,
    private cepService: ConsultaCepService,
    private verificaEmailService: VerificaEmailService) {
      super();
    }

  ngOnInit() {

    /* this.dropdownService.getEstadosBr()
      .subscribe((dados: any) => {
        this.estados = dados
        console.log(dados);
      }); */

    // subscribe automatico com pipe async
    //this.estados = this.dropdownService.getEstadosBr();
    this.dropdownService.getEstadosBr()
      .subscribe(dados => this.estados = dados);

    this.cargos = this.dropdownService.getCargos();
    this.tecnologias = this.dropdownService.getTecnologias();
    this.newsletterOp = this.dropdownService.getNewsletter();
    //this.verificaEmailService.verificarEmail('email@email.com').subscribe();


    /* this.form = new FormGroup({
      nome: new FormControl(null),
      email: new FormControl(null)
    }); */

    this.form = this.formBuilder.group({
      nome: [null, [Validators.required, Validators.minLength(3), Validators.maxLength(35)]],
      email: [null, [Validators.required, Validators.email], [this.validarEmail.bind(this)]],
      confirmarEmail: [null, FormValidations.equalsTo('email')],
      endereco: this.formBuilder.group({
        cep: [null, [Validators.required, FormValidations.cepValidator]],
        numero: [null, Validators.required],
        complemento: [null],
        rua: [null, Validators.required],
        bairro: [null, Validators.required],
        cidade: [null, Validators.required],
        estado: [null, Validators.required],
      }),
      cargo: [null],
      tecnologias: [null],
      newsletter: ['s'],
      termos: [null, Validators.pattern('true')],
      frameworks: this.buildFrameworks()
    });

    this.form.get('endereco.cep').statusChanges
      .pipe(
        distinctUntilChanged(),
        tap(value => console.log('valor CEP: ', value)),
        switchMap(status => status === 'VALID' ?
          this.cepService.consultaCEP(this.form.get('endereco.cep').value)
          : empty())
      )
      .subscribe(dados => dados ? this.populaDadosForm(dados) : {})

    this.form.get('endereco.estado').valueChanges
      .pipe(
        tap(estado => console.log('Novo estado: ', estado)),
        map(estado => this.estados.filter(e => e.sigla === estado)),
        map(estados => estados && estados.length > 0 ? estados[0].id : empty() ),
        switchMap((estadoId: number) => this.dropdownService.getCidades(estadoId)),
        tap(console.log)
      )
      .subscribe(cidades => this.cidades = cidades);

  }

  buildFrameworks() {
    const values = this.frameworks.map(v => new FormControl(false));

    return this.formBuilder.array(values, FormValidations.requiredMinCheckbox(1));
  }

 /*  requiredMinCheckbox(min = 1) {
    const validator = (formArray: FormArray) => {
     /*  const values = formArray.controls;
      let totalChecked = 0;

      for ( let i = 0; i < values.length; i++ ) {
        if (value[i].value)
          totalChecked += 1;
      } *

      const totalChecked = formArray.controls
        .map(v => v.value)
        .reduce((total, current) => current ? total + current : total, 0);

      return totalChecked >= min ? null : { required: true };
    };

    return validator;
  } */


  submit()  {
    console.log(this.form.value);

    let valueSubmit = Object.assign({}, this.form.value);

    valueSubmit = Object.assign(valueSubmit, {
        frameworks: valueSubmit.frameworks
          .map((v, i) => v ?  this.frameworks[i] : null )
          .filter(v => v !== null )
    });

    console.log(valueSubmit);

    this.http.post('https://httpbin.org/post', JSON.stringify(valueSubmit))
      .subscribe(dados => {
        console.log(dados);

        // reseta o form
        this.form.reset();
      }, (error: any) => alert('erro'));
  }

  /* onSubmit() {
    /* console.log(this.form.value);

    let valueSubmit = Object.assign({}, this.form.value);

    valueSubmit = Object.assign(valueSubmit, {
        frameworks: valueSubmit.frameworks
          .map((v, i) => v ?  this.frameworks[i] : null )
          .filter(v => v !== null )
    });

    console.log(valueSubmit); *

    if (this.form.valid) {
      this.http.post('https://httpbin.org/post', JSON.stringify(valueSubmit))
        .subscribe(dados => {
          console.log(dados);

          // reseta o form
          this.form.reset();
        }, (error: any) => alert('erro'));
    } else {
      /* console.log('formulario invalido');
      Object.keys(this.form.controls).forEach((campo) => {
        console.log(campo);
        const control = this.form.get(campo);
        control.markAsDirty();
      }); *

      this.verificaValidacoesForm(this.form);
    }
  } */

  /* verificaValidacoesForm(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach((campo) => {
      console.log(campo);
      const control = formGroup.get(campo);
      control.markAsDirty();

      if (control instanceof FormGroup) {
        this.verificaValidacoesForm(control);
      }
    });
  } */

  /* resetar() {
    this.form.reset();
  } */

  /* verificaValidTouched(campo: string) {
    return !this.form.get(campo).valid && (this.form.get(campo).touched || this.form.get(campo).dirty)
  }

  verificaRequired(campo: string) {
    return (
      this.form.get(campo).hasError('required') &&
      !this.form.get(campo).valid && (this.form.get(campo).touched || this.form.get(campo).dirty)
    )
  }

  verificaEmailInvalido() {
    let campoEmail = this.form.get('email');
    if (campoEmail.errors) {
      return campoEmail.errors['email'] && campoEmail.touched;
    }
  }

  aplicaCssErro(campo) {
    return {
      'is-invalid': this.verificaValidTouched(campo)
    }
  } */

  consultaCEP() {
    let cep = this.form.get('endereco.cep').value;

    if (cep != null && cep !== '') {
      this.cepService.consultaCEP(cep)
        .subscribe(dados => this.populaDadosForm(dados));
    }
  }

  populaDadosForm(dados) {
    this.form.patchValue({
      endereco: {
        cep: dados.cep,
        complemento: dados.complemento,
        rua: dados.logradouro,
        bairro: dados.bairro,
        cidade: dados.localidade,
        estado: dados.uf
      }
    });
  }

  resetaDadosForm() {
    this.form.patchValue({
      endereco: {
        complemento: null,
        rua: null,
        bairro: null,
        cidade: null,
        estado: null
      }
    })
  }

  setarCargo() {
    const cargo = { nome: 'Dev', nivel: 'Pleno', desc: 'Dev Pl'};

    this.form.get('cargo').setValue(cargo);
  }

  compararCargos(obj1, obj2) {
    return obj1 && obj2 ? (obj1.nome === obj2.nome && obj1.nivel === obj2.nivel) : obj1 === obj2;
  }

  setarTecnologias() {
    this.form.get('tecnologias').setValue(['java', 'javascript', 'php']);
  }

  validarEmail(formControl: FormControl) {
    return this.verificaEmailService.verificarEmail(formControl.value)
      .pipe(
        map(emailExiste => emailExiste ? { emailInvalido: true} : null)
      );
  }
}
