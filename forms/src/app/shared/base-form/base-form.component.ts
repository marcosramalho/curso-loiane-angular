import { Component, OnInit } from '@angular/core';
import { FormGroup, FormArray } from '@angular/forms';

@Component({
  selector: 'app-base-form',
  template: '<div></div>'
})
export abstract class BaseFormComponent implements OnInit {

  public form: FormGroup;

  constructor() { }

  ngOnInit() {
  }

  abstract submit();

  onSubmit() {
    if (this.form.valid) {
      this.onSubmit();
    } else {
      this.verificaValidacoesForm(this.form);
    }
  }

  verificaValidacoesForm(formGroup: FormGroup | FormArray) {
    Object.keys(formGroup.controls).forEach((campo) => {
      console.log(campo);
      const control = formGroup.get(campo);
      control.markAsDirty();
      control.markAsTouched();

      if (control instanceof FormGroup || control instanceof FormArray) {
        this.verificaValidacoesForm(control);
      }
    });
  }

  resetar() {
    this.form.reset();
  }

  verificaValidTouched(campo: string) {
    return !this.form.get(campo).valid && (this.form.get(campo).touched || this.form.get(campo).dirty)
  }

  verificaRequired(campo: string) {
    return (
      this.form.get(campo).hasError('required') &&
      !this.form.get(campo).valid && (this.form.get(campo).touched || this.form.get(campo).dirty)
    )
  }

  verificaEmailInvalido() {
    let campoEmail = this.form.get('email');
    if (campoEmail.errors) {
      return campoEmail.errors['email'] && campoEmail.touched;
    }
  }

  aplicaCssErro(campo) {
    return {
      'is-invalid': this.verificaValidTouched(campo)
    }
  }
}
