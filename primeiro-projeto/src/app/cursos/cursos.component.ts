import { CursosService } from './cursos.service';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-cursos',
  templateUrl: './cursos.component.html',
  styleUrls: ['./cursos.component.css']
})
export class CursosComponent implements OnInit {

  nomePortal: string;
  /* cursos: string[] = ['Java', 'Ext JS', 'Angular']; */
  cursos: string[];

  // injeção de dependências
  constructor(private cursosService: CursosService) {
    this.nomePortal = "http://loiane.training";

    // dados vindo do service
    /* var servico = new CursosService(); */
    this.cursos = this.cursosService.getCursos();

  }

  ngOnInit() {
  }

}
