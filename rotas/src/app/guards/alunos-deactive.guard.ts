import { Injectable } from '@angular/core';
import { RouterStateSnapshot, ActivatedRouteSnapshot, CanDeactivate } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { AlunoFormComponent } from '../alunos/aluno-form/aluno-form.component';
import { IFormCanDeactivate } from './iform-candeactivate.guard';


@Injectable()
//export class AlunosDeactivateGuard implements CanDeactivate<AlunoFormComponent> {
  export class AlunosDeactivateGuard implements CanDeactivate<IFormCanDeactivate> {
  
    
    canDeactivate(
      //component: AlunoFormComponent,
      component: IFormCanDeactivate,
      route: ActivatedRouteSnapshot,
      state: RouterStateSnapshot
    ): Observable<boolean>|Promise<boolean>|boolean {
      
      console.log('guarda de rota deactive');

      //return component.podeMudarRota(); 
      
      return component.podeDesativar();
  }
}
