import { NgModule } from '@angular/core';

import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './guards/auth.guard';
import { CursosGuard } from './guards/cursos.guard';
import { PaginaNaoEncontradaComponent } from './pagina-nao-encontrada/pagina-nao-encontrada.component';
//import { AlunosGuard } from './guards/alunos.guard';
//import { CursosComponent } from './cursos/cursos.component';
//import { CursoDetalheComponent } from './cursos/curso-detalhe/curso-detalhe.component';
//import { CursoNaoEncontradoComponent } from './cursos/curso-nao-encontrado/curso-nao-encontrado.component';


const appRoutes: Routes = [
  // criando um lazy load
  { path: 'cursos', loadChildren: 'app/cursos/cursos.module#CursosModule', 
                    canActivate: [AuthGuard], 
                    canActivateChild: [CursosGuard],
                    canLoad: [AuthGuard] },
  { path: 'alunos', loadChildren: 'app/alunos/alunos.module#AlunosModule',
                    canActivate: [AuthGuard], //canActivateChild: [AlunosGuard] --> transferido para dentro do model alunos},
                    canLoad: [AuthGuard]},
  //{path: 'cursos', component: CursosComponent},
  //{path: 'curso/:id', component: CursoDetalheComponent}, // rota com parâmetro
  { path: 'login', component: LoginComponent}, // caminho da rotas
  //{path: 'naoEncontrado', component: CursoNaoEncontradoComponent},
  { path: 'home', component: HomeComponent, canActivate: [AuthGuard]}, // caminho da rotas
  { path: '', redirectTo: '/home', pathMatch: 'full' }, // redirecionando rota padrão
  { path: '**', component: PaginaNaoEncontradaComponent}, //canActivate: [AuthGuard] }

];


@NgModule({
  imports: [RouterModule.forRoot(appRoutes, {useHash: true})], // usado para rotas raiz      
  exports: [RouterModule]
})

export class AppRoutingModule {

}