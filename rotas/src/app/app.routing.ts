import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { CursosComponent } from './cursos/cursos.component';
import { CursoDetalheComponent } from './cursos/curso-detalhe/curso-detalhe.component';
import { CursoNaoEncontradoComponent } from './cursos/curso-nao-encontrado/curso-nao-encontrado.component';


// configurando as rotas
const APP_ROUTES: Routes = [
  { path: '', component: HomeComponent }, // caminho da rotas
  { path: 'cursos', component: CursosComponent},
  { path: 'curso/:id', component: CursoDetalheComponent }, // rota com parâmetro
  { path: 'login', component: LoginComponent }, // caminho da rotas
  { path: 'naoEncontrado', component: CursoNaoEncontradoComponent },
  
];


// contem a configuração das rotas do projeto
export const routing: ModuleWithProviders = RouterModule.forRoot(APP_ROUTES);

