// GUARDA DE ROTA QUE RESOLVE O CARREGAMENTO DE DADOS ANTES DA ROTA SER ATIVADA, CLICKADA, ETC.
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Resolve } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { Aluno } from '../aluno';
import { AlunosService } from '../alunos.service';



@Injectable()
export class AlunoDetalheResolver implements Resolve<Aluno> {
  
  constructor(private alunosService: AlunosService) {}
  
  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
    ): Observable<any>|Promise<any>|any {
      
      console.log("AlunoDetalhResolver: Rota de carregamento de dados");

      let id = route.params['id'];

      return this.alunosService.getAluno(id);
  }
}
