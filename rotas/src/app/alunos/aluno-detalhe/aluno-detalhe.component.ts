import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Rx';
import { ActivatedRoute, Router } from '@angular/router';
import { AlunosService } from '../alunos.service';
import { Aluno } from '../aluno';



@Component({
  selector: 'app-aluno-detalhe',
  templateUrl: './aluno-detalhe.component.html',
  styleUrls: ['./aluno-detalhe.component.css']
})
export class AlunoDetalheComponent implements OnInit {

  aluno: Aluno;
  inscricao: Subscription;

  constructor(private route: ActivatedRoute,private router: Router, private alunosService: AlunosService) { }

  ngOnInit() {
    /* Foi omitido para usar o rota resolver
    this.inscricao = this.route.params.subscribe(
      (params: any) => {
        let id = params['id'];

        this.aluno = this.alunosService.getAluno(id);
      }
    );
 */
    console.log('ngOnInit: ALunoDetalheComponemt');

    this.inscricao = this.route.data.subscribe((params: {aluno: Aluno} ) => {
      console.log('Recebendo o obj Aluno do resolver');
      
      this.aluno = params.aluno;
    });
  }

  ngOnDestroy() {
    this.inscricao.unsubscribe();
  }

  editarContato() {
    this.router.navigate(['/alunos', this.aluno.id, 'editar']);
  }
}