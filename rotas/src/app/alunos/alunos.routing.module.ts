import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AlunosComponent } from './alunos.component';
import { AlunoDetalheComponent } from './aluno-detalhe/aluno-detalhe.component';
import { AlunoFormComponent } from './aluno-form/aluno-form.component';
import { AlunosGuard } from '../guards/alunos.guard';
import { AlunosDeactivateGuard } from '../guards/alunos-deactive.guard';
import { AlunoDetalheResolver } from './guards/aluno-detalhe.resolver';


const alunosRoutes = [
  //{ path: 'alunos', component: AlunosComponent, children: [ saiu para configurar lazy load
  { path: '', component: AlunosComponent, 
    canActivateChild: [AlunosGuard], 
    children: [
      { path: 'novo', component: AlunoFormComponent, canDeactivate: [AlunosDeactivateGuard] },
      { path: ':id', component: AlunoDetalheComponent, resolve: { aluno: AlunoDetalheResolver} },
      { path: ':id/editar', component: AlunoFormComponent, canDeactivate: [AlunosDeactivateGuard] },
    ]
  }

  /* Rotas comuns
  { path: 'alunos/novo', component: AlunoFormComponent },
  { path: 'alunos/:id', component: AlunoDetalheComponent },
  { path: 'alunos/:id/editar', component: AlunoFormComponent },*/
];

@NgModule({
  imports: [RouterModule.forChild(alunosRoutes)],    
  exports: [RouterModule]
})


export class AlunosRoutingModule {

}