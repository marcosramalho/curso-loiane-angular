import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute } from '@angular/router';
import { AlunosService } from '../alunos.service';
import { IFormCanDeactivate } from '../../guards/iform-candeactivate.guard';



@Component({
  selector: 'app-aluno-form',
  templateUrl: './aluno-form.component.html',
  styleUrls: ['./aluno-form.component.css']
})
export class AlunoFormComponent implements OnInit, IFormCanDeactivate {

  public aluno: any = {};
  public inscricao: Subscription;
  private formMudou: boolean = false;


  constructor(private route: ActivatedRoute, private alunosService: AlunosService) { }


  ngOnInit() {
    
    this.inscricao = this.route.params.subscribe(
      (params: any) => {
        let id = params['id'];

        this.aluno = this.alunosService.getAluno(id);

        if (this.aluno === null) {
          this.aluno = {};
        }
      }
    );

    
  }

  ngOnDestroy() {
    this.inscricao.unsubscribe();
  }

  public onInput(): void {
    this.formMudou = true;
    console.log(this.formMudou);
  }

  public podeMudarRota(): any {

    if (this.formMudou) {
      confirm('Tem certeza que deseja dessa página ?');
    }

    return true;
  }

  public podeDesativar() {
    return this.podeMudarRota();
  }

}
