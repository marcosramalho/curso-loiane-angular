import { Component, OnInit } from '@angular/core';

import { CursosService } from './cursos.service';
import { LogService } from '../shared/log.service';

@Component({
  selector: 'app-cursos',
  templateUrl: './cursos.component.html',
  styleUrls: ['./cursos.component.css'],
  providers: [CursosService] //instancia de serviço só para esse componenent
})
export class CursosComponent implements OnInit {

  cursos: string[] = [];
  //cursosService: CursosService;

  constructor(private cursosService: CursosService) { 
    //this.cursosService = new CursosService(); Forma ruim
    //this.cursosService = _cursosService; // injecçao de indepedência 
    // ou fazer como acima
  }

  ngOnInit() {
    
    this.cursos = this.cursosService.getCursos();

    // emite uma notificação, similar do youtube
    CursosService.criouNovoCurso.subscribe(
      // adiciona o curso enviado via EventEmitter para essa clase
      curso => this.cursos.push(curso)
    );
  }

}
