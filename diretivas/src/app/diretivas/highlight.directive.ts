import { Directive, HostListener, HostBinding, ElementRef, Renderer, Input } from '@angular/core';

@Directive({
  selector: '[highlight]'
})
export class HighlightDirective {

  @HostListener('mouseenter') onmouseover() {
    this.backgroundColor = this.highlightColor;
  }

  @HostListener('mouseleave') onmouLeave() {
    this.backgroundColor = this.defaultColor;
  }

  @HostBinding('style.backgroundColor') backgroundColor: string; 
  

  @Input() defaultColor: string = 'white';
  @Input('highlight') highlightColor: string = 'yellow';

  constructor() {}

  // quando inicia o component
  ngOnInit() {
    this.backgroundColor = this.defaultColor;
  }
}
