import { Component, OnInit, Input, EventEmitter, Output, ViewChild, ElementRef } from '@angular/core';



@Component({
  selector: 'contador',
  templateUrl: './output-property.component.html',
  styleUrls: ['./output-property.component.css']
  //outputs:['mudouValor']
})
export class OutputPropertyComponent implements OnInit {


  // recebe um valor do pai para o filho
  @Input() valor: number = 0;

  // do filho para o pai
  @Output() mudouValor = new EventEmitter();

  // pegando valor do input
  @ViewChild('campoInput') campoValorInput: ElementRef;

  constructor() { }

  ngOnInit() {
  }

  incrementa() {
    this.campoValorInput.nativeElement.value++;
    /* this.valor++; */
    this.mudouValor.emit({novoValor: this.valor});
  }

  decrementa() {
    this.campoValorInput.nativeElement.value++;
    /* this.valor--; */
    this.mudouValor.emit({novoValor: this.valor});
  }

}
