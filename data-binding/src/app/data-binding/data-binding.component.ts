import { KeyboardEvent } from 'ng2-bootstrap/utils/facade/browser';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-data-binding',
  templateUrl: './data-binding.component.html',
  styleUrls: ['./data-binding.component.css']
})
export class DataBindingComponent implements OnInit {

  url: string = 'http://www.loiane.com';
  cursoAngular: boolean = true;
  urlImagem: string = 'http://lorempixel.com/400/200/nature/';
  
  valorAtual: string = '';
  valorSalvo: string = '';

  isMouseOver: boolean = false;
  
  nomeDoCurso: string = 'Angular';


  valorInicial: number = 15;

  constructor() { }

  ngOnInit() {
  }

  getValor() {
    return 1; 
  }

  getCurtirCurso() {
    return true;
  }

  botaoClicado() {
    alert('Botão clicado!');
  }

  onKeyUp(event: KeyboardEvent) {
    this.valorAtual = (<HTMLInputElement>event.target).value;
  }

  salvarValor(valor) {
    this.valorSalvo = valor;
  }

  onMouseOverOut() {
    this.isMouseOver = !this.isMouseOver;
  }

  onMudouValor(evento) {
    console.log(evento.novoValor)
  }
}
