import { Subject } from 'rxjs';
import { Component, OnInit, Input } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-confirm-modal',
  templateUrl: './confirm-modal.component.html',
  styleUrls: ['./confirm-modal.component.scss']
})
export class ConfirmModalComponent implements OnInit {

  @Input() public title: string;
  @Input() public msg: string;
  @Input() public cancelTxt: string = 'Cancelar';
  @Input() public okTxt: string = 'Sim';

  public confirmResult: Subject<boolean>;

  constructor(private bsModalRef: BsModalRef) { }

  ngOnInit() {
    this.confirmResult = new Subject<boolean>();
  }

  public onConfirm() {
   this.confirmAndClose(true);
  }

  public onClose() {
    this.confirmAndClose(false);
  }

  private confirmAndClose(value: boolean) {
    this.confirmResult.next(value);
    this.bsModalRef.hide();    
  }

}
