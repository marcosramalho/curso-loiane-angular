import { Injectable } from '@angular/core';

import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EnviarValorService {

  // cria um observable global usando Subject
  private emissor$ = new Subject<string>();

  constructor() { }

  public emitiValor(valor: string) {
    // emite um novo valor subject
    this.emissor$.next(valor);
  }

  getValor(): Observable<string> {
    // retorna o subject como observable
    return this.emissor$.asObservable();
  }


}
