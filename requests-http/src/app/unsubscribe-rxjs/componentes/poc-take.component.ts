import { tap, take } from 'rxjs/operators';
import { Component, OnInit, OnDestroy } from '@angular/core';

import { EnviarValorService } from '../enviar-valor.service';

@Component({
  selector: 'app-poc-take',  
  template: `
    <app-poc-base [nome]="nome" [valor]="valor" estilo="bg-info"></app-poc-base>
  `
})
export class PocTakeComponent implements OnInit, OnDestroy {

  nome = "Componente com Take";
  valor: string;


  constructor(private enviarValorService: EnviarValorService) { }

  ngOnInit() {
    this.enviarValorService.getValor()
    .pipe(
      tap(v => console.log(this.nome, v)),
      take(1) // faz apenas uma request e encerra
    )
    .subscribe(novoValor => this.valor = novoValor);
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    console.log(`${this.nome} foi destruido`);
  }

}
