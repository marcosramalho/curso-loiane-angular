import { Component, OnInit, OnDestroy } from '@angular/core';

import { EnviarValorService } from '../enviar-valor.service';
import { Subject} from 'rxjs';
import { tap, takeUntil,  } from 'rxjs/operators';

@Component({
  selector: 'app-poc-take-until',  
  template: `
    <app-poc-base [nome]="nome" [valor]="valor" estilo="bg-primary"></app-poc-base>
  `
})
export class PocTakeUntilComponent implements OnInit, OnDestroy {

  nome = "Componente com TakeUntil";
  valor: string;

  unsub$ = new Subject();

  constructor(private enviarValorService: EnviarValorService) { }

  ngOnInit() {
    this.enviarValorService.getValor()
    .pipe(      
      tap(v => console.log(this.nome, v)),
      takeUntil(this.unsub$) // faz o unsubscribe automaticamente
    )
    .subscribe(novoValor => this.valor = novoValor);
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    // evita o memory leak
    this.unsub$.next();
    this.unsub$.complete();
    console.log(`${this.nome} foi destruido`);
  }

}
