
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap, delay, take } from 'rxjs/operators';

import { Curso } from './curso';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CursosService {

  private readonly API = `${environment.API}cursos` ;

  constructor(private http: HttpClient) { }

  public list(): Observable<Curso[]> {
    return this.http.get<Curso[]>(this.API)
      .pipe(
        delay(2000),
        tap(console.log)
      )
  }

  public loadById(id) {
    return this.http.get<Curso>(`${this.API}/${id}`)
      .pipe(
        take(1)
      )
  }

  private create(curso) {
    return this.http.post(this.API, curso)
      .pipe(
        delay(2000),
        tap(console.log),
        take(1)
      )
  }

  private update(curso) {
    return this.http.put(`${this.API}/${curso.id}`, curso).pipe(take(1));
  }

  public save(curso) {
    if (curso.id) {
      return this.update(curso);
    } 

    return this.create(curso);
  }

  public remove(id: number) {
    return this.http.delete(`${this.API}/${id}`).pipe(take(1));
  }

  
}
