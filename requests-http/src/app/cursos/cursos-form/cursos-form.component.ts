import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { CursosService } from '../cursos.service';
import { AlertModalService } from 'src/app/shared/alert-modal.service';
import { map, switchMap } from 'rxjs/operators';
import { Cursos2Service } from '../cursos2.service';


@Component({
  selector: 'app-cursos-form',
  templateUrl: './cursos-form.component.html',
  styleUrls: ['./cursos-form.component.scss']
})
export class CursosFormComponent implements OnInit {

  public form: FormGroup;
  public submitted: boolean = false;
  
  constructor(
    private formBuilder: FormBuilder, 
    private cursosService: Cursos2Service,
    private alertModalService: AlertModalService,
    private location: Location,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {

    /*this.route.params.subscribe((params: any) => {
      const id = params['id'];      
      const curso$ = this.cursosService.loadById(id);
      curso$.subscribe(curso => {
        this.updateForm(curso);   
      });
    });*/

    // refactory do codigo acima
    /*this.route.params
    .pipe(
      map((params: any) => params['id']),
      switchMap(id => this.cursosService.loadById(id)),
      //switchMap(id => obterCursos) --> exemplo
    )
    .subscribe(curso => this.updateForm(curso));*/
        
    // concatMap --> ordem da requisicao importa
    // mergeMao --> ordem não importa
    // exhaustMap --> casos de login

    // refactoring do codigo acima utilizando o RESOLVE
    const curso = this.route.snapshot.data['curso'];
  
    this.form = this.formBuilder.group({
      id: this.formBuilder.control(curso.id),
      nome: this.formBuilder.control(curso.nome, [Validators.required, Validators.minLength(3), Validators.maxLength(255)])
    });
  }

  /*updateForm(curso) { 
    this.form.patchValue({
      nome: curso.nome,
      id: curso.id
    });
  }*/

  hasError(field: string) {
    return this.form.get(field).errors;
  }

  onSubmit() {
    this.submitted = true;

    console.log(this.form.value)
    if (this.form.valid) { 
      console.log('submit');

      let msgSuccess = "Curso Criado com sucesso!";
      let msgError = "Erro ao criar curso, tente novamente!"
      if (this.form.value.id) {
        msgSuccess = "Curso atualizado com sucesso!";
        msgError = "Erro ao atualizar curso, tente novamente!"
      }

      this.cursosService.save(this.form.value)
      .subscribe(
        success => {
          this.alertModalService.showAlertSuccess(msgSuccess);
          this.location.back();
        },
        error => this.alertModalService.showAlertDanger(msgError),
        () => console.log('request complete')
      ); 

     /* if (this.form.value.id) {
        //update
        this.cursosService.update(this.form.value)
        .subscribe(
          success => {
            this.alertModalService.showAlertSuccess('Curso atualizado com sucesso!');
            this.location.back();
          },
          error => this.alertModalService.showAlertDanger('Erro ao atualizar curso, tente novamente!'),
          () => console.log('request complete')
        ); 
      } else {
        this.cursosService.create(this.form.value)
        .subscribe(
          success => {
            this.alertModalService.showAlertSuccess('');
            this.location.back();
          },
          error => this.alertModalService.showAlertDanger(''),
          () => console.log('request complete')
        )
      }*/
     
    }
  }

  onCancel() {
    this.submitted = false;
    this.form.reset();
  }

}
