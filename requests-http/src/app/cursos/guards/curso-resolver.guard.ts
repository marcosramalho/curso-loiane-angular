import { CursosService } from '../cursos.service';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot,Resolve } from '@angular/router';
import { Observable, of } from 'rxjs';

import { Curso } from './../curso';

@Injectable({
  providedIn: 'root'
})
export class CursoResolverGuard implements Resolve<Curso>  {
  
  
  constructor(private service: CursosService) {}
  
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Curso> {
    
    if (route.params && route.params['id']) {
      // se existir, traz o curso existente
      return this.service.loadById(route.params['id']);
    } 


    // se não existe curso, será criado um novo
    return of({
      id: null,
      nome: null
    });

  }
} 
