import { Component, OnInit, ViewChild } from '@angular/core';

import { Observable, empty, Subject, EMPTY } from 'rxjs';
import { catchError, take, switchMap } from 'rxjs/operators';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { Curso } from './../curso';
import { AlertModalService } from 'src/app/shared/alert-modal.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Cursos2Service } from '../cursos2.service';

@Component({
  selector: 'app-cursos-lista',
  templateUrl: './cursos-lista.component.html',
  styleUrls: ['./cursos-lista.component.scss'],
  preserveWhitespaces: true
})
export class CursosListaComponent implements OnInit {

  //cursos: Curso[];  
  cursos$: Observable<Curso[]>; //variavel de observable
  error$ = new Subject<boolean>();
  
  public deleteModalRef: BsModalRef;
  @ViewChild('deleteModal') public deleteModal;

  public cursoSelecionado: Curso;

  constructor(private cursosService: Cursos2Service, 
    private modalService: BsModalService,
    private alertModalService: AlertModalService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    /*this.cursosService.list()
    .subscribe((cursos: Curso[]) => {
      this.cursos = cursos;
    }); */
    
    this.onRefresh();
  }

  onRefresh() {
    // GET ASYNC
    this.cursos$ = this.cursosService.list()
    .pipe(
      catchError(error => {
        console.error(error);
        this.error$.next(true);
        this.handleError();
        return empty();
      })
    );
    
  }
  
  handleError() {    
   /* this.bsModalRef = this.modalService.show(AlertModalComponent);
    this.bsModalRef.content.type = "danger";
    this.bsModalRef.content.message = "Erro ao carregar cursos. Tente novamente mais tarde.";*/

    this.alertModalService.showAlertDanger('Erro ao carregar cursos. Tente novamente mais tarde.');
  }

  onEdit(id: number) {
    this.router.navigate(['editar', id], { relativeTo: this.route});
  }

  onDelete(curso: Curso) {
    this.cursoSelecionado = curso;
    //this.deleteModalRef = this.modalService.show(this.deleteModal, {class: 'modal-sm'});

    const result$ = this.alertModalService.showConfirm('Confirmação', 'Tem certeza em remover esse curso?');
    result$.asObservable()
    .pipe(
      take(1),
      switchMap(result => result ? this.cursosService.remove(curso.id) : EMPTY)      
    )
    .subscribe(
      success => {
        this.onRefresh()
      },
      error => {        
        this.alertModalService.showAlertDanger('Erro ao remover curso. Tente novamente mais tarde.')
      }
    )
  }

  onConfirmDelete() {
    this.cursosService.remove(this.cursoSelecionado.id)
    .subscribe(
      success => {
      this.onRefresh()
      },
      error => {
        this.deleteModalRef.hide();
        this.alertModalService.showAlertDanger('Erro ao remover curso. Tente novamente mais tarde.')
      }
    )
  }
  
}
 